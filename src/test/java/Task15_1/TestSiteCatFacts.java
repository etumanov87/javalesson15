package Task15_1;


import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.hamcrest.Matchers.*;

public class TestSiteCatFacts {

    RequestSpecification requestSpecBuilder = new RequestSpecBuilder()
            .setBaseUri("https://catfact.ninja")
            .log(LogDetail.ALL)
            .build();
    ResponseSpecification responseSpecBuilder = new ResponseSpecBuilder()
            .log(LogDetail.ALL)
            .expectContentType(ContentType.JSON)
            .expectStatusCode(200)
            .build();

    @Test(description = "Проверка, что значение поле {breed} равно Aegean для второго элемента массива {data}")
    public void checkBodyContainsValuesForSelectedFieldForBreeds() {
        RestAssured
                .given()
                .spec(requestSpecBuilder)
                .when()
                .get("/breeds")
                .then()
                .spec(responseSpecBuilder)
                .body("data[1].breed", equalTo("Aegean"));
    }

    @Test(description = "Проверка, что поле {breed} заполнено для все списка объектов")
    public void isNotEmptyFieldBreedForBreeds() {
        boolean expectedResult = false;
        boolean actualResult =
                RestAssured
                        .given()
                        .spec(requestSpecBuilder)
                        .when()
                        .get("/breeds")
                        .then()
                        .spec(responseSpecBuilder)
                        .extract()
                        .body()
                        .jsonPath()
                        .getList("data", Breeds.class)
                        .stream()
                        .map(b -> b.breed)
                        .toList().contains("");
        Assert.assertEquals(actualResult,expectedResult,"Ошибка!!! Нету информации о {breed} кошки");
    }

    @Test(description = "Проверка, что данные первого элемента массива {data} совпадают с ожидаемыми")
    public void checkFirstElementDataWhatExpectedForBreeds() {
        Breeds expectedBreeds = new Breeds("Abyssinian", "Ethiopia", "Natural/Standard", "Short", "Ticked");
        Breeds actualBreeds = RestAssured
                .given()
                .spec(requestSpecBuilder)
                .when()
                .get("/breeds")
                .then()
                .spec(responseSpecBuilder)
                .extract().body().jsonPath().getObject("data[0]", Breeds.class);
        Assert.assertEquals(expectedBreeds, actualBreeds);
    }

    @Test(description = "Проверить что в ответ указана правильная {length} рандомного факта")
    public void checkFactLengthMatchesExpectedLengthForFact() {
        Facts randomFact = RestAssured
                .given()
                .spec(requestSpecBuilder)
                .when()
                .get("/fact")
                .then()
                .spec(responseSpecBuilder)
                .extract()
                .body()
                .jsonPath().getObject("", Facts.class);
        int expectedFactLength = randomFact.length;
        int actualFactLength = randomFact.fact.length();
        Assert.assertEquals(actualFactLength, expectedFactLength);
    }

    @DataProvider(name = "factsForChecking")
    public Object[][] dataProviderForSpecifiedFactsPresentInResponseBody() {
        return new Object[][]{
                {"The technical term for a cat’s hairball is a “bezoar.”", true},
                {"A group of cats is called a “clowder.”", true},
                {"", false},
                {"Approximately 24 cat skins...", false}
        };
    }

    @Test(dataProvider = "factsForChecking",
            description = "Проверить что заданные факты присутствуют в теле ответа")
    public void checkSpecifiedFactsPresentInResponseBodyForFacts(String fact, boolean expectedResult) {
        boolean actualResult = RestAssured
                .given()
                .spec(requestSpecBuilder)
                .when()
                .get("/facts")
                .then()
                .spec(responseSpecBuilder)
                .extract()
                .body()
                .jsonPath()
                .getList("data", Facts.class)
                .stream()
                .map(f -> f.fact)
                .toList()
                .contains(fact);
        Assert.assertEquals(actualResult, expectedResult);
    }
}
