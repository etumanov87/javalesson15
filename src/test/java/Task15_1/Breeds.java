package Task15_1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Breeds {
    public String breed;
    public String country;
    public String origin;
    public String coat;
    public String pattern;
}
